# 1000ideas


## Getting started

```
cd existing_folder
git clone origin https://gitlab.com/piotrdziubczynski/1000ideas.git .
```

## Installation

```
docker-compose up
```

## Before usage

```
docker-compose exec php sh
composer install
php bin/console doctrine:migration:migrate
```

## How to run tests
```
docker-compose exec php sh
php bin/console --env=test doctrine:database:create
php bin/console --env=test doctrine:schema:create
php bin/phpunit tests
```
