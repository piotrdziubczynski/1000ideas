<?php

declare(strict_types=1);

namespace App\Tests\Service\Generator;

use App\Service\Generator\GeneratorInterface;
use App\Service\Generator\SimpleHashGeneratorService;
use PHPUnit\Framework\TestCase;

class SimpleHashGeneratorServiceTest extends TestCase
{
    private GeneratorInterface $hashGenerator;
    protected function setUp(): void
    {
        $this->hashGenerator = new SimpleHashGeneratorService();
    }

    public function testShouldReturnEmptyString(): void
    {
        $hashed = $this->hashGenerator->generate('');

        self::assertEmpty($hashed);
    }

    public function testShouldReturnCorrectHashedString(): void
    {
        $string = 'test';
        $hashed = $this->hashGenerator->generate($string);

        self::assertEquals(32, strlen($hashed));
        self::assertSame(md5($string), $hashed);
    }
}
