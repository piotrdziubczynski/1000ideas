<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use ApiTestCase\JsonApiTestCase;
use App\Entity\Product;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManager;

class ProductControllerTest extends JsonApiTestCase
{
    private string $path = '/product/';

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        $repository = static::getContainer()->get('doctrine')->getRepository(Product::class);

        foreach ($repository->findAll() as $object) {
            $repository->remove($object, true);
        }

        /** @var EntityManager $em */
        $em = static::getContainer()->get('doctrine')->getManager();
        $connection = $em->getConnection();

        $connection->executeStatement('ALTER TABLE `product` AUTO_INCREMENT = 1;');
    }

    public function testCreate(): void
    {
        $this->client->request(
            'POST', $this->path, [], [],
            [],
            '{ "name": "test", "quantity": 99 }'
        );

        $response = $this->client->getResponse();

        self::assertResponseStatusCodeSame(201);
        self::assertJsonStringEqualsJsonString(
            '{"product":{"id":1,"name":"test","hash":"098f6bcd4621d373cade4e832627b4f6","quantity":99}}',
            $response->getContent()
        );
    }

    public function testShow(): void
    {
        $this->client->request(
            'POST', $this->path, [], [],
            [],
            '{ "name": "test", "quantity": 99 }'
        );

        $this->client->request('GET', $this->path . '1');

        $response = $this->client->getResponse();

        self::assertResponseStatusCodeSame(200);
        self::assertJsonStringEqualsJsonString(
            '{"product":{"id":1,"name":"test","hash":"098f6bcd4621d373cade4e832627b4f6","quantity":99}}',
            $response->getContent()
        );
    }

    public function testDelete(): void
    {
        $this->client->request(
            'POST', $this->path, [], [],
            [],
            '{ "name": "test", "quantity": 99 }'
        );

        $this->client->request('DELETE', $this->path . '1');

        $response = $this->client->getResponse();

        self::assertResponseStatusCodeSame(200);
        self::assertJsonStringEqualsJsonString('{"product":{"id":1}}', $response->getContent());
    }
}
