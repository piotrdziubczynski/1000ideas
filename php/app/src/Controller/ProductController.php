<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Product;
use App\Model\Product\Write\ProductWriteModel;
use App\Service\Generator\GeneratorInterface;
use App\Service\Product\ProductService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route('/product')]
class ProductController extends AbstractController
{
    public function __construct(
        private readonly GeneratorInterface $generator,
        private readonly ProductService $productService,
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator
    ) {
    }

    #[Route('/', name: 'app_product_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        /** @var ProductWriteModel $productModel */
        $productModel = $this->serializer->deserialize(
            $request->getContent(),
            ProductWriteModel::class,
            'json',
            ['groups' => 'add_product']
        );

        $productModel->setHash($this->generator->generate($productModel->getName()));

        $errors = $this->validator->validate($productModel);

        if ($errors->count() > 0) {
            $firstError = $errors->get(0);

            return new JsonResponse([
                'error' => $firstError->getMessage(),
                'propertyPath' => $firstError->getPropertyPath(),
                'errorCode' => $firstError->getCode(),
            ], Response::HTTP_BAD_REQUEST);
        }

        $product = $this->productService->addProduct($productModel);
        $json = $this->serializer->serialize($product, 'json', ['groups' => 'show_product']);

        return new JsonResponse([
            'product' => json_decode($json, true, JSON_PRETTY_PRINT)
        ], Response::HTTP_CREATED);
    }

    #[Route('/{id}', name: 'app_product_show', methods: ['GET'])]
    public function show(Request $request, Product $product): JsonResponse
    {
        $json = $this->serializer->serialize($product, 'json', ['groups' => 'show_product']);

        return new JsonResponse([
            'product' => json_decode($json, true, JSON_PRETTY_PRINT)
        ], Response::HTTP_OK);
    }

    #[Route('/{id}', name: 'app_product_delete', methods: ['DELETE'])]
    public function delete(Request $request, Product $product): JsonResponse
    {
        $productId = $product->getId();
        $this->productService->deleteProduct($product);

        return new JsonResponse(['product' => ['id' => $productId]], Response::HTTP_OK);
    }
}
