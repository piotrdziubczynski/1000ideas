<?php

declare(strict_types=1);

namespace App\Service\Product;

use App\Entity\Product;
use App\Model\Product\Write\ProductWriteModel;
use App\Repository\ProductRepository;

final class ProductService
{
    public function __construct(
        private readonly ProductRepository $repository
    ) {
    }

    public function addProduct(ProductWriteModel $model): Product
    {
        $product = new Product($model->getName());

        $product->setHash($model->getHash());
        $product->setQuantity($model->getQuantity());

        $this->repository->save($product, true);

        return $this->repository->findOneBy([
            'hash' => $product->getHash(),
        ]);
    }

    public function deleteProduct(Product $product): void
    {
        $this->repository->remove($product, true);
    }
}
