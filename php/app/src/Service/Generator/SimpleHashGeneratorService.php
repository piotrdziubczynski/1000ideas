<?php

declare(strict_types=1);

namespace App\Service\Generator;

final class SimpleHashGeneratorService implements GeneratorInterface
{
    public function generate(string $string): string
    {
        if ($string === '') {
            return '';
        }

        return md5($string);
    }
}
