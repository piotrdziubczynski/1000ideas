<?php

declare(strict_types=1);

namespace App\Model\Product\Write;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

final class ProductWriteModel
{
    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Assert\Length(
        min: 3,
        max: 255
    )]
    #[Groups(['add_product'])]
    private string $name;

    #[Assert\NotBlank]
    #[Assert\Type('string')]
    #[Assert\Length(
        exactly: 32
    )]
    #[Groups(['add_product'])]
    private string $hash;

    #[Assert\NotBlank]
    #[Assert\Type('int')]
    #[Assert\GreaterThanOrEqual(1)]
    #[Assert\LessThanOrEqual(999)]
    #[Groups(['add_product'])]
    private int $quantity = 1;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): void
    {
        $this->hash = $hash;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }
}
